/**
 *
 */
package org.prelle.splimo.charctrl4;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface SpliMoCharGenConstants {

	public final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/splimo/chargen/i18n/splittermond/chargen");

}
