package de.rpgframework.splittermond.print.json.model;

import java.util.List;

public class JSONSkill {
    public String name;
    public String id;
    public String attribute1;
    public String attribute2;
    public int value;
    public int points;
    public int modifier;
    public List<JSONMastership> masterships;
}
